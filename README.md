# Laravel Migrations
* * *
Repositório criado com o objetivo de explicar para que serve e como funciona as migrations no laravel.

De forma rápida e simples, gostaria de explicar para que serve, vamos lá:

No mundo da programação sabemos que há como realizar o **versionamento** dos sistemas, de forma evolutiva como no exemplo abaixo:

* 0.1.0 (Alpha)
* 0.1.1 (Alpha)
* 1.0.0
* 1.1.0
* 1.1.1
* (e por ai vai...)

Sempre seguindo os conceitos de minors e majors na hora de realizar um **versionamento** propriamente dito, porém, você já pensou como eu mantenho um histórico 
de versão da minha/sua base de dados?

Exemplo:

* 0.1.0 - Criado a tabela de clientes
* 0.1.1 - Alterado a tabela de clientes para adicionar mais um campo
* 1.0.0 - Criado uma tabela para endereço do cliente
* 1.1.0 - Removido um campo da tabela de clientes
* 1.1.1 - Criado uma tabela para telefones do cliente
* (e por ai vai...)

Pensando por este lado, bem que seria interessante criarmos um mecanismo que registre o histórico das etapas de atualização do banco de dados para que de 
forma simples, possamos migrar a nossa base de dados para uma versão específica do sistema.

Imagine que você fornece o seu sistema para determinados clientes, e que você tem alguns de seus clientes com versões específicas que não são as mais atualizadas, 
porém estes mesmos clientes te reportam problemas de banco de dados, persistência, etc. Como você vai simular este problema se você nem tem em sua base de dados 
atual o esquema/estrutura do banco de dados daquela versão específica do seu sistema?

Para isso que foi criado as migrations, elas servem para você manter um histórico de aplicação e reversão da sua base de dados que de forma automática você
pode atualizar ou reverter para uma versão específica, seja para realizar testes ou uma correção minor.

* * *

Sem mais delongas, vamos criar nossa primeira migration (primeira versão do banco de dados).

As Migrations possuem por padrão 2 métodos, que são Up e o Down, que servem respectivamente para aplicar e reverter em sequência (de acordo com a versão que você se encontra)

Para criar uma migration, você deve acessar o


```php
<?php

use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;


class CreateFlightsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
	 
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('airline');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('flights');
    }
}
```




